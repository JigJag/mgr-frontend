import React, { PureComponent } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ReplayIcon from '@material-ui/icons/Replay';
import ClearIcon from '@material-ui/icons/Clear';
import GetAppIcon from '@material-ui/icons/GetApp';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
      flexGrow: 1,
      height: '8%',
      background: '#4b4b4b',
      color: '#17ce17'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    menuIcon: {
      fontSize: '130%'
    },
    title: {
      flexGrow: 1,
      fontSize: '130%'
    },
  });

export class TopBar extends PureComponent {

    handleDrawer = () => {
      this.props.dispatch({type: 'switchDrawer'});
    }

    discard = () => {
      this.props.dispatch({type: 'discard'});
    }

    revert = () => {
      this.props.dispatch({type: 'revert'});
    }

    download = () => {
      this.props.download();
    }

    render() {
        const { classes } = this.props;
        if (this.props.isImageLoaded) {
          return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar className={classes.root}>
                      <Typography variant="h6" className={classes.title}>
                          {this.props.title}
                      </Typography>
                      <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                                  onClick={this.download}>
                          <GetAppIcon className={classes.menuIcon} />
                      </IconButton>
                      <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                                  onClick={this.discard}>
                          <ClearIcon className={classes.menuIcon} />
                      </IconButton>
                      <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                                  onClick={this.revert}>
                          <ReplayIcon className={classes.menuIcon} />
                      </IconButton>
                      <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                                  onClick={this.handleDrawer}>
                          <MenuIcon className={classes.menuIcon} />
                      </IconButton>
                    </Toolbar>
                </AppBar>
            </div>
          )
        } else {
          return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar className={classes.root}>
                      <Typography variant="h6" className={classes.title}>
                          {this.props.title}
                      </Typography>
                      <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                                  onClick={this.handleDrawer}>
                          <MenuIcon className={classes.menuIcon} />
                      </IconButton>
                    </Toolbar>
                </AppBar>
            </div>
          )
        }
        
    }
}

export default withStyles(styles) (TopBar)
