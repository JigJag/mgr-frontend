import React from 'react'
import List from '@material-ui/core/List';
import Collapse from '@material-ui/core/Collapse';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core';
import AspectRatioTwoToneIcon from '@material-ui/icons/AspectRatioTwoTone';
import FilterTwoToneIcon from '@material-ui/icons/FilterTwoTone';
import TextFieldsTwoToneIcon from '@material-ui/icons/TextFieldsTwoTone';
import PhotoSizeSelectSmallTwoToneIcon from '@material-ui/icons/PhotoSizeSelectSmallTwoTone';
import GridOffTwoToneIcon from '@material-ui/icons/GridOffTwoTone';
import RotateLeftTwoToneIcon from '@material-ui/icons/RotateLeftTwoTone';
import AppsTwoToneIcon from '@material-ui/icons/AppsTwoTone';
import BlurCircularTwoToneIcon from '@material-ui/icons/BlurCircularTwoTone';
import EmojiObjectsTwoToneIcon from '@material-ui/icons/EmojiObjectsTwoTone';
import EmojiObjectsRoundedIcon from '@material-ui/icons/EmojiObjectsRounded';
import FilterBAndWRoundedIcon from '@material-ui/icons/FilterBAndWRounded';
import SettingsBrightnessRoundedIcon from '@material-ui/icons/SettingsBrightnessRounded';
import VignetteTwoToneIcon from '@material-ui/icons/VignetteTwoTone';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const styles = makeStyles(theme => ({
    root: {
      background: '#424242',
      color: '#17ce17',
      height: '87.8vh'
    },

    listItem: {
        background: '#424242',
    },

    icon: {
        color: '#17ce17',
        fontSize: '130%'
    },

    nested: {
        background: '#363636'
    }
}));

function tabReducer(state, action) {
    switch(action.type) {
        case 'tab1':
            return {tab1: !state.tab1, tab2: false, tab3: false}
        case 'tab2':
            return {tab1: false, tab2: !state.tab2, tab3: false}
        case 'tab3':
            return {tab1: false, tab2: false, tab3: !state.tab3}
        default:
            return {tab1: false, tab2: false, tab3: false}
    }
}
  
function ToolsList(props) {

    let initState = {tab1: false, tab2: false, tab3: false};
    
    const [state, dispatch] = React.useReducer(tabReducer, initState);

    const classes = styles();

    const handleToolSelection = (selectedTool) => {
        props.dispatch({type: 'switchDrawer'});
        props.handleToolbar({type: 'toolbar', payload: selectedTool});
    }

    return (
        <div>
            <List className={classes.root}>
                <ListItem button onClick={() => dispatch({type: 'tab1'})} className={classes.listItem}>
                    <ListItemIcon>
                        <AspectRatioTwoToneIcon className={classes.icon}/>
                    </ListItemIcon>
                    <ListItemText primary={'Transformacje'} />
                    {state.tab1 ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={state.tab1} timeout="auto" unmountOnExit>
                    <List disablePadding className={classes.nested}>
                        <ListItem button onClick={() => handleToolSelection('scale')}>
                            <ListItemIcon>
                                <PhotoSizeSelectSmallTwoToneIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Skalowanie" />
                        </ListItem>
                        <ListItem button onClick={() => handleToolSelection('crop')}>
                            <ListItemIcon>
                                <GridOffTwoToneIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Przycinanie" />
                        </ListItem>
                        <ListItem button onClick={() => handleToolSelection('rotation')}>
                            <ListItemIcon>
                                <RotateLeftTwoToneIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Obracanie" />
                        </ListItem>
                    </List>
                </Collapse>

                <ListItem button onClick={() => dispatch({type: 'tab2'})}  className={classes.listItem}>
                    <ListItemIcon>
                        <FilterTwoToneIcon className={classes.icon}/>
                    </ListItemIcon>
                    <ListItemText primary='Filtry' />
                    {state.tab2 ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={state.tab2} timeout="auto" unmountOnExit>
                    <List disablePadding className={classes.nested}>
                        <ListItem button onClick={() => handleToolSelection('blur')}>
                            <ListItemIcon>
                                <BlurCircularTwoToneIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Rozmycie" />
                        </ListItem>
                        <ListItem button onClick={() => handleToolSelection('sharpen')}>
                            <ListItemIcon>
                                <AppsTwoToneIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Wyostrzanie" />
                        </ListItem>
                        <ListItem button onClick={() => handleToolSelection('light')}>
                            <ListItemIcon>
                                <EmojiObjectsRoundedIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Rozjaśnianie" />
                        </ListItem>
                        <ListItem button onClick={() => handleToolSelection('dim')}>
                            <ListItemIcon>
                                <EmojiObjectsTwoToneIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Przyciemnianie" />
                        </ListItem>
                        <ListItem button onClick={() => handleToolSelection('contrast')}>
                            <ListItemIcon>
                                <SettingsBrightnessRoundedIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Kontrast" />
                        </ListItem>
                        <ListItem button onClick={() => handleToolSelection('sepia')}>
                            <ListItemIcon>
                                <FilterBAndWRoundedIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary="Sepia" />
                        </ListItem>
                        <ListItem button onClick={() => handleToolSelection('vignette')}>
                            <ListItemIcon>
                                <VignetteTwoToneIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText primary='Winieta' />
                        </ListItem>
                    </List>
                </Collapse>

                <ListItem button onClick={() => handleToolSelection('text')} className={classes.listItem}>
                    <ListItemIcon>
                        <TextFieldsTwoToneIcon className={classes.icon}/>
                    </ListItemIcon>
                    <ListItemText primary='Tekst' />
                </ListItem>
            </List>
        </div>
        
    )
}

export default ToolsList