import React from 'react'
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ToolsList from './ToolsList';
import { makeStyles } from '@material-ui/core';

const drawerWidth = 260;

const styles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: drawerWidth,
    background: '#424242',
    color: '#17ce17',
    fontSize: '150%'
  },

  drawerHeader: {
    width: drawerWidth,
    background: '#424242',
    color: '#17ce17'
  },

  icon: {
    position: 'relative',
    left: 30,
    color: '#17ce17',
    fontSize: '200%'
  },

  chevron: {
    fontSize: '100%'
  }
}))

function PersistentDrawer(props) {

    const classes = styles();

    const handleDrawer = () => {
      props.dispatch({type: 'switchDrawer'});
    }

    return (
        <Drawer
        className={classes.root}
        variant="persistent"
        anchor="right"
        open={props.isDrawerOpen}
      >
        <div className={classes.drawerHeader}>
          Narzędzia
          <IconButton onClick={handleDrawer} className={classes.icon}>
            <ChevronRightIcon className={classes.chevron}/>
          </IconButton>
        </div>
        <Divider />
        <ToolsList dispatch={handleDrawer} handleToolbar={props.handleToolbar}/>
      </Drawer>
    )
}

export default PersistentDrawer
