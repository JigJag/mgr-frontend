import React from 'react';
import { useReducer } from 'react'
import './App.css';
import TopBar from './TopBar/TopBar';
import PersistentDrawer from './Drawer/PersistentDrawer'
import MainPanel from './MainPanel/MainPanel'
import Footer from './Common/Footer/Footer'
import { CallApi } from './Common/ApiCalls'
import Message from './Common/Message'

//padding for image container - used for initial scaling
const canvasPadding = 40;
//max allowed file size to upload (MB)
const maxFileSize = ( 10 ) * 1048576;

const initialState = {
  isDrawerOpen: false,
  isModalOpen: false,
  canEdit: false,
  isImageLoaded: false,
  imageState: null,
  imageSrc: null,
  imageName: null,
  prevImageSrc: null,
  cropData: null,
  modalMessage: null,
  toolbar: null
};

const appReducer = (appState, action) => {
  switch(action.type) {
      case 'switchDrawer':
          return {...appState, isDrawerOpen: !appState.isDrawerOpen}
      case 'switchModal':
          return {...appState, isModalOpen: action.payload, modalMessage: action.message}
      case 'setCropData':
          return {...appState, cropData: action.payload}
      case 'checkEdit':
          return {...appState, canEdit: action.payload}
      case 'checkImageLoaded':
          return {...appState, isImageLoaded: action.payload}
      case 'checkImageCropping':
          return {...appState, imageState: 'cropping'}
      case 'checkImageProcessing':
          if (action.payload)
            return {...appState, imageState: 'processing'}
          else
            return {...appState, imageState: 'loaded'}
      case 'imageSrc':
          return {...appState, imageSrc: action.payload}
      case 'imageName':
          return {...appState, imageName: action.payload}
      case 'setPrev':
          return {...appState, prevImageSrc: appState.imageSrc,}
      case 'toolbar':
          if (action.payload === 'crop')
            return {...appState, prevImageSrc: appState.imageSrc, toolbar: 'crop', imageState: 'cropping'}
          else
            return {...appState, prevImageSrc: appState.imageSrc, toolbar: action.payload}
      case 'discard':
            return initialState
      case 'revert':
          return {...appState, imageSrc: appState.prevImageSrc}
      default:
          return appState
  }
}

function App() {

  const [appState, dispatch] = useReducer(appReducer, initialState);

  const showLoading = () => {
      dispatch({type: 'checkImageProcessing', payload: true});
      dispatch({type: 'checkImageLoaded', payload: true});
  }

  const handleResponse = (response) => {
    if(response.ok) {
        dispatch({type: 'imageSrc', payload: response.image});
        dispatch({type: 'setPrev'});
        dispatch({type: 'checkImageProcessing', payload: false});
        dispatch({type: 'checkEdit', payload: true});
    } else {
        dispatch({type: 'switchModal', payload: true, message: response.message});
        dispatch({type: 'checkImageProcessing', payload: false});
        dispatch({type: 'checkImageLoaded', payload: false});
    }
  }

  const handleFile = (file) => {
    if ( /\.(jpe?g|png|bmp)$/i.test(file.name) ) {
        if (file.size >= maxFileSize)
          dispatch({type: 'switchModal', 
                    payload: true, 
                    message: 'Maksymalny rozmiar obrazu to ' + maxFileSize / 1048576 + 'MB'
                  });
        else {
          const fileReader = new FileReader();
          fileReader.onload = (e) => {
              dispatch({type: 'imageName', payload: file.name.replace(/\.[^/.]+$/, "")});
              const containerWidth = document.getElementById('canvas').clientWidth - canvasPadding;
              const containerHeight = document.getElementById('canvas').clientHeight - canvasPadding;
              CallApi('api/Initial', {
                imageData: fileReader.result,
                containerWidth: containerWidth,
                containerHeight: containerHeight
              }, showLoading, handleResponse);
          }
          fileReader.readAsDataURL(file);
        }
    }
  }

  const downloadFile = () => {
    var file = new Blob([appState.imageSrc], {type: ".png"});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, appState.imageName);
    else { // Others
        var a = document.createElement("a");
        a.href = appState.imageSrc;
        a.download = appState.imageName + '.png';
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a); 
        }, 0); 
    }
  }

  return (
    <div className="App">
      <Message isOpen={appState.isModalOpen} text={appState.modalMessage} dispatch={dispatch}/>
      <PersistentDrawer isDrawerOpen={appState.isDrawerOpen} 
                        dispatch={dispatch} 
                        handleToolbar={appState.canEdit ? dispatch : ()=>{}}/>
      <TopBar title="Edytor Obrazów" dispatch={dispatch} isImageLoaded={appState.isImageLoaded}
              download={downloadFile}/>
      <MainPanel isImageLoaded={appState.isImageLoaded} imageState={appState.imageState}
                 imageSrc={appState.imageSrc} dispatch={dispatch} canvasPadding={canvasPadding}
                 toolbarState={appState.toolbar} handleFile={handleFile} cropData={appState.cropData}/>
      <Footer/>
    </div>
  );
}

export default App;