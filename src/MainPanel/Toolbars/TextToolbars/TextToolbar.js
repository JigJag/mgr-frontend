import React, { useState } from 'react'
import CustomButton from '../../../Common/CustomButton/CustomButton'
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { CallApi } from '../../../Common/ApiCalls';

const CssTextField = withStyles({
    root: {
      paddingBottom: '20px',
      paddingRight: '20px',
      color: '#17ce17',
      '& label.Mui-focused': {
        color: '#17ce17',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#17ce17',
      },
      '& .MuiOutlinedInput-root': {
        '&.Mui-focused fieldset': {
          borderColor: '#17ce17',
        },
      },
    },
})(TextField);

function TextToolbar(props) {

    const [upperText, setUpperText] = useState("");
    const [lowerText, setLowerText] = useState("");

    const showLoading = () => {
        props.dispatch({type: 'checkImageProcessing', payload: true});
    }
  
    const handleResponse = (response) => {
        if(response.ok) {
          props.dispatch({type: 'imageSrc', payload: response.image});
          props.dispatch({type: 'checkImageProcessing', payload: false});
        } else {
          props.dispatch({type: 'switchModal', payload: true, message: response.message});
          props.dispatch({type: 'checkImageProcessing', payload: false});
        }
    }

    const setUpper = (event) => {
      setUpperText(event.target.value);
    }

    const setLower = (event) => {
      setLowerText(event.target.value);
    }

    const handleChange = () => {
      if (upperText == "" || lowerText == "")
        props.dispatch({type: 'switchModal', payload: true, message: 'Wprowadź najpierw tekst'});
      else {
        CallApi('api/ApplyText', {
            imageData: props.imageSrc,
            upperText: upperText,
            lowerText: lowerText
          }, showLoading, handleResponse);
      }
    }

    return (
        <div style={{color: '#17ce17', fontSize: '130%'}}>
            <CssTextField
                label="GÓRNY TEKST"
                variant="outlined"
                multiline={true}
                value={upperText}
                onChange={setUpper}
            />
            <CssTextField
                label="DOLNY TEKST"
                variant="outlined"
                multiline={true}
                value={lowerText}
                onChange={setLower}
            />
            <CustomButton label={'Zatwierdź'} onClick={handleChange}/>
        </div>
    )
}

export default TextToolbar
