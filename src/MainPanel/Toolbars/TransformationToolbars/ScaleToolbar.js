import React from 'react'
import { useState } from 'react'
import { CallApi } from '../../../Common/ApiCalls';
import PrettoSlider from '../../../Common/PrettoSlider';

const marks = [
    {
        value: 25,
        label: '-75%',
    },
    {
        value: 50,
        label: '-50%',
    },
    {
        value: 75,
        label: '-25%',
    },
    {
        value: 100,
        label: '0%',
    },
    {
        value: 125,
        label: '+25%',
    },
    {
        value: 150,
        label: '+50%',
    },
    {
        value: 175,
        label: '+75%',
    },
    {
        value: 200,
        label: '+100%',
    },
];

function ScaleToolbar(props) {

    const [value, setValue] = useState(100);
    const [image, setImage] = useState(props.imageSrc);

    const showLoading = () => {
      props.dispatch({type: 'checkImageProcessing', payload: true});
    }

    const handleResponse = (response) => {
      if(response.ok) {
        props.dispatch({type: 'imageSrc', payload: response.image});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      } else {
        props.dispatch({type: 'switchModal', payload: true, message: response.message});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      }
    }

    const setNewValue = (event, newValue) => {
      setValue(newValue);
    }

    const handleChange = () => {
      // check if scaling is necessary
        let img = new Image();
        img.src = image;
        let doNotRescale = false;
        const containerWidth = document.getElementById('canvas').clientWidth - props.canvasPadding;
        const containerHeight = document.getElementById('canvas').clientHeight - props.canvasPadding;
        if (value > 100)
          if (img.width >= containerWidth || img.height >= containerHeight) {
            doNotRescale = true;
            props.dispatch({type: 'switchModal', payload: true, message: 'Obraz ma już maksymalny rozmiar'});
          }
            
      //
        if (!doNotRescale) {
          CallApi('api/Scale', {
            imageData: image,
            containerWidth: containerWidth,
            containerHeight: containerHeight,
            percent: value
          }, showLoading, handleResponse);
        }
    }
    
    return (
        <div style={{color: '#17ce17', fontSize: '130%'}}>
            Skalowanie
            <PrettoSlider value={value} onChange={setNewValue} onChangeCommitted={handleChange} 
                          marks={marks} min={25} max={200}
            />
        </div>
    )
}

export default ScaleToolbar
