import React from 'react'
import CustomButton from '../../../Common/CustomButton/CustomButton'
import { CallApi } from '../../../Common/ApiCalls';

function CropToolbar(props) {

    const showLoading = () => {
        props.dispatch({type: 'checkImageProcessing', payload: true});
    }
  
    const handleResponse = (response) => {
        if(response.ok) {
          props.dispatch({type: 'imageSrc', payload: response.image});
          props.dispatch({type: 'checkImageCropping'});
        } else {
          props.dispatch({type: 'switchModal', payload: true, message: response.message});
          props.dispatch({type: 'checkImageProcessing', payload: false});
        }
    }

    const handleChange = () => {
        CallApi('api/Crop', {
            imageData: props.imageSrc,
            x: props.cropData.x,
            y: props.cropData.y,
            width: props.cropData.width,
            height: props.cropData.height
          }, showLoading, handleResponse);
    }

    return (
        <div style={{color: '#17ce17', fontSize: '130%'}}>
            Zaznacz obszar
            <CustomButton label={'PRZYTNIJ'} onClick={handleChange}/>
        </div>
    )
}

export default CropToolbar
