import React from 'react'
import { useState } from 'react'
import { CallApi } from '../../../Common/ApiCalls';
import PrettoSlider from '../../../Common/PrettoSlider';

const marks = [
    {
        value: -180,
        label: '-180°',
    },
    {
        value: -90,
        label: '-90°',
    },
    {
        value: 0,
        label: '0°',
    },
    {
        value: 90,
        label: '90°',
    },
    {
        value: 180,
        label: '180°',
    },
];

function RotationToolbar(props) {

    const [value, setValue] = useState(0);
    const [image, setImage] = useState(props.imageSrc);

    const showLoading = () => {
      props.dispatch({type: 'checkImageProcessing', payload: true});
    }

    const handleResponse = (response) => {
      if(response.ok) {
        props.dispatch({type: 'imageSrc', payload: response.image});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      } else {
        props.dispatch({type: 'switchModal', payload: true, message: response.message});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      }
    }

    const setNewValue = (event, newValue) => {
      setValue(newValue);
    }

    const handleChange = () => {
      const containerWidth = document.getElementById('canvas').clientWidth - props.canvasPadding;
      const containerHeight = document.getElementById('canvas').clientHeight - props.canvasPadding;
      CallApi('api/Rotate', {
          imageData: image,
          containerWidth: containerWidth,
          containerHeight: containerHeight,
          degrees: value
        }, showLoading, handleResponse);
    }
    
    return (
        <div style={{color: '#17ce17', fontSize: '130%'}}>
            Obrót
            <PrettoSlider
              value={value}
              onChangeCommitted={handleChange}
              onChange={setNewValue}
              marks={marks}
              min={-180}
              max={180}
            />
        </div>
    )
}

export default RotationToolbar
