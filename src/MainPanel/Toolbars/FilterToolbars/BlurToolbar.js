import React from 'react'
import { useState } from 'react'
import { CallApi } from '../../../Common/ApiCalls';
import PrettoSlider from '../../../Common/PrettoSlider';

function BlurToolbar(props) {

    const [value, setValue] = useState(0);
    const [image, setImage] = useState(props.imageSrc);

    const showLoading = () => {
      props.dispatch({type: 'checkImageProcessing', payload: true});
    }

    const handleResponse = (response) => {
      if(response.ok) {
        props.dispatch({type: 'imageSrc', payload: response.image});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      } else {
        props.dispatch({type: 'switchModal', payload: true, message: response.message});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      }
    }

    const setNewValue = (event, newValue) => {
      setValue(newValue);
    }

    const handleChange = () => {
        CallApi('api/Blur', {
            imageData: image,
            value: value
        }, showLoading, handleResponse);
    }
    
    return (
        <div style={{color: '#17ce17', fontSize: '130%'}}>
            Rozmycie
            <PrettoSlider value={value} onChange={setNewValue} onChangeCommitted={handleChange} 
                          min={0} max={10} step={0.1}
            />
        </div>
    )
}

export default BlurToolbar
