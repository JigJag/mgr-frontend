import React from 'react'
import { useState } from 'react'
import { CallApi } from '../../../Common/ApiCalls';
import PrettoSlider from '../../../Common/PrettoSlider';

function SepiaToolbar(props) {

    const [sepia, setSepia] = useState(0);
    const [image, setImage] = useState(props.imageSrc);

    const showLoading = () => {
      props.dispatch({type: 'checkImageProcessing', payload: true});
    }

    const handleResponse = (response) => {
      if(response.ok) {
        props.dispatch({type: 'imageSrc', payload: response.image});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      } else {
        props.dispatch({type: 'switchModal', payload: true, message: response.message});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      }
    }

    const setSep = (event, newValue) => {
      setSepia(newValue);
    }

    const handleChange = () => {
        CallApi('api/Sepia', {
            imageData: image,
            value: sepia
        }, showLoading, handleResponse);
    }
    
    return (
        <div style={{color: '#17ce17', fontSize: '130%'}}>
            Sepia
            <PrettoSlider value={sepia} onChange={setSep} onChangeCommitted={handleChange} 
                          min={70} max={100} step={0.5}
            />
        </div>
    )
}

export default SepiaToolbar
