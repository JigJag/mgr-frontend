import React from 'react'
import { useState } from 'react'
import { CallApi } from '../../../Common/ApiCalls';
import PrettoSlider from '../../../Common/PrettoSlider';

function ContrastToolbar(props) {

    const [contrast, setContrast] = useState(0);
    const [image, setImage] = useState(props.imageSrc);

    const showLoading = () => {
      props.dispatch({type: 'checkImageProcessing', payload: true});
    }

    const handleResponse = (response) => {
      if(response.ok) {
        props.dispatch({type: 'imageSrc', payload: response.image});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      } else {
        props.dispatch({type: 'switchModal', payload: true, message: response.message});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      }
    }

    const setContr = (event, newValue) => {
      setContrast(newValue);
    }

    const handleChange = () => {
        CallApi('api/Contrast', {
            imageData: image,
            value: contrast
        }, showLoading, handleResponse);
    }
    
    return (
        <div style={{color: '#17ce17', fontSize: '130%'}}>
            Kontrast
            <PrettoSlider value={contrast} onChange={setContr} onChangeCommitted={handleChange} 
                          min={0} max={100} step={0.5}
            />
        </div>
    )
}

export default ContrastToolbar
