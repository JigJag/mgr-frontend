import React from 'react'
import { useState } from 'react'
import { CallApi } from '../../../Common/ApiCalls';
import { withStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import PrettoSlider from '../../../Common/PrettoSlider';

const GreenCheckbox = withStyles({
    root: {
      color: '#17ce17',
      '&$checked': {
        color: '#17ce17',
      },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

function VignetteToolbar(props) {

    const [radius, setRadius] = useState(0);
    const [width, setWidth] = useState(0);
    const [height, setHeight] = useState(0);
    const [color, setColor] = useState('black');
    const [checked, setChecked] = useState({checkedA: true, checkedB: false});
    const [image, setImage] = useState(props.imageSrc);

    const showLoading = () => {
      props.dispatch({type: 'checkImageProcessing', payload: true});
    }

    const handleResponse = (response) => {
      if(response.ok) {
        props.dispatch({type: 'imageSrc', payload: response.image});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      } else {
        props.dispatch({type: 'switchModal', payload: true, message: response.message});
        props.dispatch({type: 'checkImageProcessing', payload: false});
      }
    }

    const setRad = (event, newValue) => {
      setRadius(newValue);
    }

    const setWid = (event, newValue) => {
      setWidth(newValue);
    }

    const setHei = (event, newValue) => {
      setHeight(newValue);
    }

    const handleCheck = (event) => {
        if (event.target.name === 'checkedA') {
            setChecked({checkedA: true, checkedB: false});
            setColor('black');
        } else {
            setChecked({checkedA: false, checkedB: true});
            setColor('white');
        }
      };

    const handleChange = () => {
        CallApi('api/Vignette', {
            imageData: image,
            radius: radius,
            width: width,
            height: height,
            color: color
        }, showLoading, handleResponse);
    }
    
    return (
        <div style={{color: '#17ce17', fontSize: '130%'}}>
            <p>Winieta</p>
            Miękkość
            <PrettoSlider value={radius} onChange={setRad} onChangeCommitted={handleChange} 
                          min={0} max={100} step={0.5}
            />
            Szerokość
            <PrettoSlider value={width} onChange={setWid} onChangeCommitted={handleChange} 
                          min={-20} max={80}
            />
            Wysokość
            <PrettoSlider value={height} onChange={setHei} onChangeCommitted={handleChange} 
                          min={-20} max={80}
            />
            <GreenCheckbox checked={checked.checkedA} onChange={handleCheck} name="checkedA" />
            Ciemna
            <GreenCheckbox checked={checked.checkedB} onChange={handleCheck} name="checkedB" />
            Jasna
        </div>
    )
}

export default VignetteToolbar
