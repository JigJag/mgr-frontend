import React from 'react'
import ScaleToolbar from './Toolbars/TransformationToolbars/ScaleToolbar'
import CropToolbar from './Toolbars/TransformationToolbars/CropToolbar'
import RotationToolbar from './Toolbars/TransformationToolbars/RotationToolbar'
import BlurToolbar from './Toolbars/FilterToolbars/BlurToolbar'
import SharpenToolbar from './Toolbars/FilterToolbars/SharpenToolbar'
import LightToolbar from './Toolbars/FilterToolbars/LightToolbar'
import DimToolbar from './Toolbars/FilterToolbars/DimToolbar'
import ContrastToolbar from './Toolbars/FilterToolbars/ContrastToolbar'
import SepiaToolbar from './Toolbars/FilterToolbars/SepiaToolbar'
import TextToolbar from './Toolbars/TextToolbars/TextToolbar'
import VignetteToolbar from './Toolbars/FilterToolbars/VignetteToolbar'

function ToolbarContainer(props) {
    
    switch(props.toolbar) {
        case 'scale' :
            return <ScaleToolbar
                        dispatch={props.dispatch}
                        canvasPadding={props.canvasPadding} 
                        imageSrc={props.imageSrc}
                    />
        case 'crop' :
            return <CropToolbar
                        dispatch={props.dispatch}
                        imageSrc={props.imageSrc}
                        cropData={props.cropData}
                   />
        case 'rotation' :
            return <RotationToolbar
                        dispatch={props.dispatch}
                        canvasPadding={props.canvasPadding} 
                        imageSrc={props.imageSrc}
                    />
        case 'blur' :
            return <BlurToolbar
                        dispatch={props.dispatch} 
                        imageSrc={props.imageSrc}
                    />
        case 'sharpen' :
            return <SharpenToolbar
                        dispatch={props.dispatch} 
                        imageSrc={props.imageSrc}
                    />
        case 'light' :
            return <LightToolbar
                        dispatch={props.dispatch} 
                        imageSrc={props.imageSrc}
                    />
        case 'dim' :
            return <DimToolbar
                        dispatch={props.dispatch} 
                        imageSrc={props.imageSrc}
                    />
        case 'contrast' :
            return <ContrastToolbar
                        dispatch={props.dispatch} 
                        imageSrc={props.imageSrc}
                    />
        case 'sepia' :
            return <SepiaToolbar
                        dispatch={props.dispatch} 
                        imageSrc={props.imageSrc}
                    />
        case 'vignette' :
            return <VignetteToolbar
                        dispatch={props.dispatch} 
                        imageSrc={props.imageSrc}
                    />
        case 'text' :
            return <TextToolbar
                        dispatch={props.dispatch} 
                        imageSrc={props.imageSrc}
                    />
        default:
            return null
    }
}

export default ToolbarContainer
