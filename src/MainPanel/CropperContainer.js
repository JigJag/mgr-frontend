import React, { useRef } from 'react'
import Cropper from 'react-cropper'
import 'cropperjs/dist/cropper.css'

function CropperContainer(props) {
    const cropper = useRef(null);

    const handleData = () => {
        props.dispatch({type: 'setCropData', payload: cropper.current.getData(true)});
    }

    return (
        <Cropper
            ref={cropper}
            src={props.imageSrc}
            crop={handleData}
        />
    )
}

export default CropperContainer
