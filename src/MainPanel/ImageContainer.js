import React from 'react'
import ScaleLoader from 'react-spinners/ScaleLoader'
import CropperContainer from './CropperContainer'

function ImageContainer(props) {

    const styles = {
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%'
    }

    const imageState = props.imageState;

    if (imageState === 'loaded') {
        return <img id={'image'} src={props.imageSrc} alt='loadedImage' style={styles}></img>
    } else if (imageState === 'processing') {
        return <ScaleLoader
                    color={'#17ce17'}
                    height={55}
                    width={7}
                    radius={3}
                    margin={3}
                />
    } else if (imageState === 'cropping') {
        return <CropperContainer
                    imageSrc={props.imageSrc}
                    dispatch={props.dispatch}
                />
    } else
        return null;
}

export default ImageContainer
