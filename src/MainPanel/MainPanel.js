import React, { useState, useEffect } from 'react'
import { makeStyles, Grid } from '@material-ui/core'
import ScreenRotationTwoToneIcon from '@material-ui/icons/ScreenRotationTwoTone'
import Canvas from './Canvas'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        height: '92%'
    },

    mainGrid: {
        justify: "center",
        alignItems: "center",
        spacing: 0
    },

    canvasContainer: {
        display: 'flex',
        justifyContent: "center",
        alignItems: "center",
        height: '76vh',
        border: '5px dashed #17ce17',
        margin: '30px',
        padding: '20px'
    },

    toolbar: {
        padding: '25px'
    },

    warning: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '76vh',
        fontSize: '200%',
        color: '#17ce17'
    },

    icon: {
        fontSize: '200%'
    }
}));

export default function MainPanel(props) {

    const [screenWidth, setWidth] = useState(window.innerWidth);

    const classes = useStyles();
    
    const throttledHandleWindowResize = function () {
        setTimeout(() => {
            setWidth(window.innerWidth);
        }, 200);
    };

    useEffect(() => {
        window.addEventListener('resize', throttledHandleWindowResize);
    }, []);

    return (
        <div className={classes.root}>
            {screenWidth < 500 ? 
                <Grid container className={classes.mainGrid}>
                    <Grid item xs={12}>
                        <div className={classes.warning}>
                            <ScreenRotationTwoToneIcon className={classes.icon}/>
                        </div>
                    </Grid>
                </Grid>
                : 
                <Canvas
                    styles={classes}
                    isImageLoaded={props.isImageLoaded}
                    imageState={props.imageState}
                    imageSrc={props.imageSrc}
                    canvasPadding={props.canvasPadding}
                    dispatch={props.dispatch}
                    handleFile={props.handleFile}
                    toolbar={props.toolbarState}
                    cropData={props.cropData}
                />
            }
        </div>
    )
}

