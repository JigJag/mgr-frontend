import React from 'react'
import { makeStyles, Grid, IconButton } from '@material-ui/core'
import AddCircleTwoToneIcon from '@material-ui/icons/AddCircleTwoTone'
import ImageContainer from './ImageContainer';
import ToolbarContainer from './ToolbarContainer';

const useStyles = makeStyles(theme => ({
    loadIcon: {
      color: '#17ce17',
      fontSize: '250%'
    }
}));

function Canvas(props) {
    
    const classes = props.styles;
    const classes2 = useStyles();
    const isImageLoaded = props.isImageLoaded;
    const handleFile = (file) => {
        props.handleFile(file);
    }

    return (
        <div>
            <Grid container className={classes.mainGrid}>
                <Grid item xs={12} md={9}>
                    <div id='canvas' className={classes.canvasContainer}>
                        {isImageLoaded ? 
                            <div className={classes.image}>
                                {<ImageContainer
                                    imageState={props.imageState}
                                    imageSrc={props.imageSrc}
                                    dispatch={props.dispatch}
                                />}
                            </div>
                            :
                            <div>
                                <input  
                                    type='file'
                                    accept='.jpg, .png, .bmp'
                                    id='input-label'
                                    style={{display: 'none'}}
                                    onChange={e => handleFile(e.target.files[0])}
                                />
                                <label htmlFor='input-label'>
                                    <IconButton component='span'>
                                        <AddCircleTwoToneIcon className={classes2.loadIcon}/>
                                    </IconButton>
                                </label>
                            </div>
                        }
                    </div>
                </Grid>
                <Grid item xs={12} md={3} className={classes.toolbar}>
                    {<ToolbarContainer
                        toolbar={props.toolbar}
                        dispatch={props.dispatch}
                        canvasPadding={props.canvasPadding}
                        imageSrc={props.imageSrc}
                        cropData={props.cropData}
                    />}
                </Grid>
            </Grid>
        </div>
    )
}

export default Canvas
