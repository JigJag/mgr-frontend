import React from 'react'
import './styles.css'

function Footer() {
    return (
        <div className='footer'>
            © Jakub Ostropolski 2020
        </div>
    )
}

export default Footer
