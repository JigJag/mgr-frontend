import React from 'react'
import './styles.css'

function CustomButton(props) {

    const handleClick = () => {
        props.onClick();
    };

    return (
        <div>
            <button onClick={handleClick}>{props.label}</button>
        </div>
    )
}

export default CustomButton
