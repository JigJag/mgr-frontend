
function CallApi(url, requestBody, preprocess, callback) {
    preprocess();
    let apiUrl = "localhost";
    if (process.env.REACT_APP_API_URL)
        apiUrl = process.env.REACT_APP_API_URL;
    fetch('http://' + apiUrl + ':5000/' + url, {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(requestBody)
    })
    .then(response => {
        if(!response.ok) {
            console.log(response.status + ' - ' + response.statusText);
        }
        return response.json();
    })
    .then(responseJson => {
        callback(responseJson);
    })
    .catch((error) => alert('Error : ' + error));
}

export { CallApi }