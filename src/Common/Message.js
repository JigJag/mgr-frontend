import React from 'react'
import Backdrop from '@material-ui/core/Backdrop';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        color: '#17ce17',
        fontSize: '150%'
    },
    backdrop: {
      zIndex: 999,
      color: '#ffffff'
    },
}));

function Message(props) {
    const classes = useStyles();
    const handleClose = () => {
        props.dispatch({type: 'switchModal', payload: false});
    };

    return (
        <Backdrop className={classes.backdrop} open={props.isOpen} onClick={handleClose}>
            <p className={classes.root}>{props.text}</p>
        </Backdrop>
    );
}

export default Message
